﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class KillCount : MonoBehaviour
{
    public int killCount;
    public GameObject CountText;
    public GameObject ShellPrefab;
    public GameObject clearText;
    public GameObject UIText;

    // Start is called before the first frame update
    void Start()
    {
        this.killCount = 0;

        //ゲームクリアテキストを非表示
        this.clearText.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (killCount >= 10)
        {
            //ステージクリア処理
            StartCoroutine("StageClear");
        }
    }

    public int KCount()
    {
        //キルカウントを1増やす
        this.killCount += 1;
        this.CountText.GetComponent<Text>().text =
this.killCount.ToString() + "/10";
        return this.killCount;
    }

    //コルーチン
    IEnumerator StageClear()
    {
        //ゲームクリアテキスト表示
        this.clearText.SetActive(true);

        //UI非表示
        this.UIText.SetActive(false);

        //3秒待つ
        yield return new WaitForSeconds(3.0f);

        //ステージセレクトへ戻る
        SceneManager.LoadScene("StageSelect");
    }
}
