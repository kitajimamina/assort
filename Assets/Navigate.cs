﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Navigate : MonoBehaviour
{
    GameObject navigate;

    // Start is called before the first frame update
    void Start()
    {
        this.navigate = GameObject.Find("Navigate");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //オブジェクトに触れた時の処理
    private void OnCollisionEnter(Collision collision)
    {
        //オブジェクトがbillboardの時
        if (gameObject.name == "billboard")
        {
            this.navigate.GetComponent<Text>().text =
                "右はアスレチック、左はFPS、\n" +
                "お帰りはまっすぐ";
        }
    }
}
