﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityChanController : MonoBehaviour
{
    //Unityちゃんのアニメーションを格納
    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        //UnityちゃんのAnimatorコンポーネントを取得
        this.animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //オブジェクトに触れた時の処理
    private void OnTriggerEnter(Collider other)
    {
        //オブジェクトがGoalの場合
        if(other.gameObject.name == "Goal")
        {
            //勝利モーションを表示
            animator.SetBool("Win", true);
        }
    }
}
