﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shooting : MonoBehaviour
{
    public GameObject bulletPrefab;
    public float shotSpeed;
    public int shotCount = 30;
    private float shotInterval;
    GameObject reload;
    public AudioClip ShotSE;
    public AudioClip ReloadWeaponsSE;
    AudioSource aud;

    // Start is called before the first frame update
    void Start()
    {
        this.reload = GameObject.Find("Reload");
        this.aud = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            shotInterval += 1;

            if (shotInterval % 2 == 0 && shotCount > 0)
            {
                shotCount -= 1;
                GameObject bullet = (GameObject)Instantiate(bulletPrefab, transform.position, Quaternion.Euler(transform.parent.eulerAngles.x, transform.parent.eulerAngles.y, 0));
                Rigidbody bulletRb = bullet.GetComponent<Rigidbody>();
                bulletRb.AddForce(transform.forward * shotSpeed);
                this.aud.PlayOneShot(this.ShotSE);

                //射撃されてから3秒後に銃弾のオブジェクトを破壊する
                Destroy(bullet, 3.0f);
            }
        }
        else if (Input.GetKeyDown(KeyCode.R)){
            shotCount = 30;
            this.aud.PlayOneShot(this.ReloadWeaponsSE);
            StartCoroutine("ReloadMessage");
        }
    }

    IEnumerator ReloadMessage()
    {
        this.reload.GetComponent<Text>().text =
    "Reloaded";

        yield return new WaitForSeconds(3.0f);

        this.reload.GetComponent<Text>().text =
    "";
    }
}
