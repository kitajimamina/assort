﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallDownController : MonoBehaviour
{
    public GameObject player;
    //初期座標
    public Vector3 startPos;

    // Start is called before the first frame update
    void Start()
    {
        //プレイヤーオブジェクトをアタッチ
        //startPos = player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //FallDownオブジェクト接触時の処理

    private void OnTriggerEnter(Collider other)
    {
        //プレイヤーが落下時
        if (other.gameObject == player)
        {
            //スタート地点に戻る
            player.transform.position = startPos;
            Debug.Log(startPos);
        }
        else
        {
            //プレイヤー以外のオブジェクトが落下した際はオブジェクトを消去
            Destroy(other.gameObject);
        }
    }

}
