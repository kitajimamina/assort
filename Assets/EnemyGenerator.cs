﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
    //生まれてくる敵プレハブ
    public GameObject enemyPrefab;

    //敵を格納
    GameObject[] existEnemys;

    //アクティブ最大数
    public int maxEnemy = 5;

    // Start is called before the first frame update
    void Start()
    {
        //配列確保
        existEnemys = new GameObject[maxEnemy];

        StartCoroutine(RepeatGenerate());
    }

    //3秒おきに敵作成を繰り返す
    IEnumerator RepeatGenerate()
    {
        while (true)
        {
            GenerateEnemy();
            yield return new WaitForSeconds(3.0f);
        }
    }


    //敵を最大数まで作成する
    void GenerateEnemy()
    {
        for(int i = 0; i < existEnemys.Length; ++i)
        {
            if(existEnemys[i] == null)
            {
                //敵を作成する
                existEnemys[i] = Instantiate(enemyPrefab) as GameObject;
                int enemyX = Random.Range(-100, 100);
                int enemyZ = Random.Range(-100, 100);
                existEnemys[i].transform.position = new Vector3(enemyX, 2, enemyZ);

                return;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
