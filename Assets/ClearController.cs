﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ClearController : MonoBehaviour
{
    public GameObject clearText;

    //コルーチン
    IEnumerator StageClear()
    {
        //ゲームクリアテキスト表示
        this.clearText.SetActive(true);

        //3秒待つ
        yield return new WaitForSeconds(3.0f);

        //ステージセレクトへ戻る
        SceneManager.LoadScene("StageSelect");
    }

    // Start is called before the first frame update
    void Start()
    {
        //ゲームクリアテキストを非表示
        this.clearText.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        //ステージクリア処理
        StartCoroutine("StageClear");
    }
}
