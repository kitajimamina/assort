﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellController : MonoBehaviour
{
    GameObject GameDirector;

    public void Shoot(Vector3 dir)
    {
        GetComponent<Rigidbody>().AddForce(dir);
    }

    private void OnCollisionEnter(Collision collision)
    {
        //敵キャラに触れた場合、相手を削除する
        if(collision.gameObject.tag == "Enemy")
        {
            GameDirector.GetComponent<KillCount>().KCount();

            Destroy(collision.gameObject);

            //自分も削除
            Destroy(this.gameObject);

        }
    }

    // Start is called before the first frame update
    void Start()
    {
        this.GameDirector = GameObject.Find("gameDirector");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
