﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageChange : MonoBehaviour
{
    public GameObject Window;
    public GameObject Navigate;

    //プレイヤーオブジェクトをアタッチ
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    //StageChangeオブジェクト接触時の処理
    private void OnTriggerEnter(Collider other)
    {
        //プレイヤーが落下時
        if(other.gameObject == player)
        {
            //メッセージを非表示
            this.Navigate.SetActive(false);
            this.Window.SetActive(false);

            if (gameObject.name == "AthleticStageChange")
            {
                SceneManager.LoadScene("Athletic");
            }
            else if(gameObject.name == "Exit")
            {
                UnityEngine.Application.Quit();
            }
            else if(gameObject.name == "FPSStageChange")
            {
                SceneManager.LoadScene("FPS");
            }
        }
    }
}
